<!doctype html>
<html lang="en">
<head>
    <title>Voertuigoverzicht - {{ substr(data_get($vehicle,'name'), 0, 2) }}-{{ substr(data_get($vehicle,'name'), 2) }}</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">

    <style>
        body {
            padding-top: 5rem;
        }

        .bg-fire {
            background-color: #d0021b;
        }

        .footer {
            margin-top: 20px;
            height: 50px;
            line-height: 50px;
            color: #999;
            text-align: center;
            background-color: #f9f9f9;
            border-top: .05rem solid #e5e5e5;
        }

        .table td, .table th {
            vertical-align: middle;
        }
    </style>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-30462054-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-30462054-2');
    </script>

    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({
            google_ad_client: "ca-pub-3983810750883422",
            enable_page_level_ads: true
        });
    </script>

</head>
<body>

<nav class="navbar navbar-expand-md navbar-dark bg-fire fixed-top">
    <a class="navbar-brand" href="https://voertuigoverzicht.nl">Voertuigoverzicht</a>
</nav>

<main role="main" class="container-fluid">

    <div class="row">
        <div class="col-md-12">

            <p>
                <a target="_blank" href="https://discord.gg/ufQzBdj">
                    <small>Wijziging doorgeven via Discord? https://discord.gg/ufQzBdj</small>
                </a>
            </p>

            <h1>{{ substr(data_get($vehicle,'name'), 0, 2) }}-{{ substr(data_get($vehicle,'name'), 2) }}</h1>

            <ul>
                <li>Nummer: {{ data_get($vehicle,'number') }}</li>
                <li>Type: {{ data_get($vehicle,'vehicle_type.name') }}</li>
                <li>Regio: {{ data_get($vehicle,'region.name') }}</li>
                <li>Kazerne: {{ data_get($vehicle,'fire_station.name') }}</li>
            </ul>

            <img src="{{ data_get($vehicle,'assets.data.0.source_file.thumbnails.576x324.url') }}" alt="{{ data_get($vehicle,'region.number') }}-{{ data_get($vehicle,'number') }}"><br />
            <i>Foto gemaakt door: {{ data_get($vehicle,'assets.data.0.author') }}</i>
        </div>
    </div>
</main>

<footer class="footer">
    <p>
        Powered by <a href="https://112pers.nl">112PERS.nl</a> & <a href="https://www.tomzulu10capcodes.nl">Tomzulu10</a>
    </p>
</footer>

</body>
</html>
