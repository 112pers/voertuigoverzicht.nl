<!doctype html>
<html lang="en">
<head>
    <title>Voertuigoverzicht</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">

    <style>
        body {
            padding-top: 5rem;
        }

        .bg-fire {
            background-color: #d0021b;
        }

        .footer {
            margin-top: 20px;
            height: 50px;
            line-height: 50px;
            color: #999;
            text-align: center;
            background-color: #f9f9f9;
            border-top: .05rem solid #e5e5e5;
        }

        .table td, .table th {
            vertical-align: middle;
        }
    </style>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-30462054-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-30462054-2');
    </script>

    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({
            google_ad_client: "ca-pub-3983810750883422",
            enable_page_level_ads: true
        });
    </script>

</head>
<body>

<nav class="navbar navbar-expand-md navbar-dark bg-fire fixed-top">
    <a class="navbar-brand" href="https://voertuigoverzicht.nl">Voertuigoverzicht</a>

    {{--<ul class="nav navbar-nav ml-auto">--}}
        {{--<li class="nav-item">--}}
            {{--<a class="nav-link" target="_blank" href="https://discord.gg/ufQzBdj">--}}
                {{--<small>Wijziging doorgeven via Discord? https://discord.gg/ufQzBdj</small>--}}
            {{--</a>--}}
        {{--</li>--}}
    {{--</ul>--}}
</nav>

<main role="main" class="container-fluid">

    <div class="row">
        <div class="col-md-12">

            <p>
                <a target="_blank" href="https://discord.gg/ufQzBdj">
                    <small>Wijziging doorgeven via Discord? https://discord.gg/ufQzBdj</small>
                </a>
            </p>

            <div class="table-responsive">
                <table id="vehicle" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Naam</th>
                        <th>Type</th>
                        <th>Kazerne</th>
                        <th>Regio</th>
                        <th>Aangepast op</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>

<footer class="footer">
    <p>
        Powered by <a href="https://112pers.nl">112PERS.nl</a> & <a href="https://www.tomzulu10capcodes.nl">Tomzulu10</a>
    </p>
</footer>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
{{--<script src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>--}}
{{--<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>--}}
{{--<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>--}}

<script>
    $(document).ready(function() {
        $('#vehicle').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": '{{ action('WebController@json') }}',
            "columns": [
                {
                    "data": "image",
                    "bSortable": false,
                    "render": function ( data, type, row ) {
                        return '<img src="' + data + '" />';
                    },
                },
                {
                    "data": "name",
                    "render": function ( data, type, row ) {
                        return '<a href="https://voertuigoverzicht.nl/' + data + '" />' + data + '</a>';
                    },
                },
                {
                    "data": "type",
                    "bSortable": false
                },
                {
                    "data": "fire_station",
                    "bSortable": false
                },
                {
                    "data": "region",
                    "bSortable": false
                },
                {
                    "data": "changed",
                    "bSortable": false
                }
            ],
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Dutch.json"
            },
            "bInfo" : false
        });
    });
</script>

</body>
</html>
