<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response as IlluminateResponse;
use Illuminate\Http\Request;

class WebController extends Controller
{
    public function json(Request $request)
    {
        $response = [];
        $total = 0;

        $orderBy = 'name';

//        if($request->input('order.0.column') == 1) {
//            $orderBy = 'name';
//        }

        $order = '-';
        if($request->input('order.0.dir') == 'asc') {
            $order = null;
        }

        $sort = $order . $orderBy;

        $query = [
            'sort' => $sort,
            'limit' => $request->input('length'),
            'offset' => $request->input('start'),
            'fields' => 'name,number,changed,region{name,number},vehicle_type{name},fire_station{name},cover,assets{source_file.thumbnails(fit/80x50),storage_files.thumbnails(fit/80x50){url}}'
        ];

        if($request->filled('search.value')) {

            $q = [
                [
                    'fz' => $request->input('search.value'),
                    'param' => 'name_format'
                ]
            ];

            data_set($query,'q',$q);

//            $fireStations = [
//                'q' => [
//                    [
//                        'fz' => $request->input('search.value'),
//                        'param' => 'name'
//                    ]
//                ]
//            ];
//
//            data_set($query,'fire_stations',$fireStations);
        }

        $apiRequest = $this->api('vehicles', 'get', $query);
        if(data_get($apiRequest,'httpStatusCode') == 200) {

            $items = data_get($apiRequest, 'response');

            if(data_get($items,'data')) {

                foreach(data_get($items,'data') as $item) {

                    $name = substr(data_get($item,'name'), 0, 2) . '-' . substr(data_get($item,'name'), 2);

                    $response[] = [
                        'uuid' => data_get($item,'uuid'),
                        'image' => (data_get($item,'assets.data.0.source_file.thumbnails.80x50.url') ? data_get($item,'assets.data.0.source_file.thumbnails.80x50.url') : ''),
                        'name' => $name,
                        'type' => data_get($item,'vehicle_type.name'),
                        'fire_station' => data_get($item,'fire_station.name'),
                        'region' => data_get($item,'region.name'),
                        'changed' => (data_get($item,'changed') ? $this->dateFromMicrotimestamp(data_get($item,'changed')) : ''),
                    ];
                }
            }

            $total = data_get($items, 'summary.total_count');
        }

        return response()->json([
//            'draw' => 1,
//            'recordsTotal' => $total,
            'recordsFiltered' => $total,
            'data' => $response
        ]);
    }

    public function index(Request $request)
    {
        return view('index');
    }

    public function show(Request $request, $name)
    {
        $name = str_replace('-','',$name);

        $query = [
            'fields' => 'name,number,changed,region{name,number},vehicle_type{name},fire_station{name},cover,assets{author,source_file.thumbnails(fit/576x324){url}}'
        ];

        $apiRequest = $this->api('vehicles/name/' . $name, 'get', $query);
        if(data_get($apiRequest,'httpStatusCode') == 200) {

            $item = data_get($apiRequest, 'response');

            return view('show',[
                'vehicle' => $item
            ]);
        }
        abort(404);
    }

    public function dateFromMicrotimestamp($microtimestamp, $format = null)
    {
        if($microtimestamp) {
            return \Carbon\Carbon::createFromTimestamp($microtimestamp/1000)->formatLocalized(($format ? $format : '%d-%m-%Y %H:%M'));
        }
    }
}
